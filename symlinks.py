#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import os


def _list_path(path, base_path=""):
    results = []
    files = os.listdir(path)
    for f in files:
        p = "%s/%s" % (path, f)
        l = "%s/%s" % (base_path, f) if base_path else f
        if os.path.isdir(p):
            results.extend(_list_path(p, l))
        else:
            results.append({
                "abspath": os.path.abspath(p),
                "local_path": l
            })
    return results


class S:
    def __init__(self, p):
        self.p = p

    def nginx_files(self):
        path = "%s/nginx/" % self.p
        files = _list_path(path)
        return files

    def httpd_files(self):
        path = "%s/httpd/" % self.p
        files = _list_path(path)
        return files


def main():
    choices = [p for p in os.listdir(".") if os.path.isdir(p) and not p.startswith(".")]
    parser = argparse.ArgumentParser(description='Symlink')
    parser.add_argument('server', type=str, choices=choices, help='server name')
    parser.add_argument('app', type=str, choices=["httpd", "nginx"], help='app name')
    args = parser.parse_args()

    s = S(args.server)

    if args.app == "httpd" and os.path.isdir("/etc/httpd/conf.d"):
        for c in s.httpd_files():
            dest = "/etc/httpd/%s" % c["local_path"]
            source = c["abspath"]

            if os.path.exists(dest):
                os.remove(dest)

            print("Create Symbol: %s -> %s" % (source, dest))
            os.symlink(source, dest)

    if args.app == "nginx" and os.path.isdir("/etc/nginx/conf.d"):
        for c in s.nginx_files():
            dest = "/etc/nginx/%s" % c["local_path"]
            source = c["abspath"]

            if os.path.exists(dest):
                os.remove(dest)

            print("Create Symbol: %s -> %s" % (source, dest))
            os.symlink(source, dest)


if __name__ == '__main__':
    main()
